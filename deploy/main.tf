terraform {
  required_providers {
    archive = {
      source  = "archive"
      version = "~> 2.3.0"
    }

    google = {
      source  = "google"
      version = "~> 4.66.0"
    }
  }
}

provider "google" {
  project = "salonfriday-dev"
  region  = "us-central1"
}

data "archive_file" "code" {
  type        = "zip"
  source_dir  = ".."
  excludes    = ["../build", "../deploy"]
  output_path = "../build/hello_world_cloud_function.zip"
}

resource "google_storage_bucket" "bucket" {
  name     = "salonfriday-dev-cloudfunctions"
  location = "US"
}

resource "google_storage_bucket_object" "archive" {
  name   = "double.zip"
  bucket = google_storage_bucket.bucket.name
  source = data.archive_file.code.output_path
}

resource "google_cloudfunctions_function" "double" {
  name                  = "double-2"
  description           = "Double cloud function"
  runtime               = "go120"
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  entry_point           = "Double"
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.double.project
  region         = google_cloudfunctions_function.double.region
  cloud_function = google_cloudfunctions_function.double.name
  role           = "roles/cloudfunctions.invoker"
  member         = "allUsers"
}

resource "google_cloudfunctions2_function" "double_v2" {
  name        = "double-v2"
  location    = "us-central1"
  description = "Double cloud function v2"

  build_config {
    runtime     = "go120"
    entry_point = "Double"

    source {
      storage_source {
        bucket = google_storage_bucket.bucket.name
        object = google_storage_bucket_object.archive.name
      }
    }
  }

  service_config {
    max_instance_count = 1
    available_memory   = "256M"
    timeout_seconds    = 60
  }
}

output "function_uri" {
  value = google_cloudfunctions2_function.double_v2.service_config[0].uri
}
