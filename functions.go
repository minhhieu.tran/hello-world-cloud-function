package functions

import (
	"net/http"

	"lingbook.io/hello-world-cloud-function/internal/logic"
	"lingbook.io/hello-world-cloud-function/internal/service"
)

var handler service.Handler

func init() {
	mathLogic := logic.NewMath()
	handler = service.NewHandler(mathLogic)
}

func Double(w http.ResponseWriter, r *http.Request) {
	handler.Double(w, r)
}
