package logic

type Math interface {
	Double(x int64) int64
}

type math struct{}

func NewMath() Math {
	return &math{}
}

func (*math) Double(x int64) int64 {
	return x * 2
}
