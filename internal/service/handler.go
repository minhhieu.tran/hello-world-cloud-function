package service

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"lingbook.io/hello-world-cloud-function/internal/logic"
)

type Handler interface {
	Double(w http.ResponseWriter, r *http.Request)
}

type handler struct {
	mathLogic logic.Math
}

func NewHandler(
	mathLogic logic.Math,
) Handler {
	return &handler{
		mathLogic: mathLogic,
	}
}

type DoubleRequest struct {
	X int64 `json:"x"`
}

type DoubleResponse struct {
	Result int64 `json:"result"`
}

func (h *handler) Double(w http.ResponseWriter, r *http.Request) {
	requestByteList, err := ioutil.ReadAll(r.Body)
	if err != nil {
		h.writeResponse(w, http.StatusBadRequest, nil)
		return
	}

	var requestBody DoubleRequest
	if err = json.Unmarshal(requestByteList, &requestBody); err != nil {
		h.writeResponse(w, http.StatusBadRequest, nil)
		return
	}

	result := h.mathLogic.Double(requestBody.X)
	h.writeResponse(w, http.StatusOK, DoubleResponse{Result: result})
}

func (h *handler) writeResponse(w http.ResponseWriter, statusCode int, body interface{}) {
	w.WriteHeader(statusCode)

	if body != nil {
		responseByteList, err := json.Marshal(body)
		if err != nil {
			return
		}

		w.Write(responseByteList)
	}
}
