package service

import (
	"fmt"

	"github.com/GoogleCloudPlatform/functions-framework-go/funcframework"
	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
)

type Server interface {
	Start()
}

type server struct {
	handler Handler
}

func NewServer(handler Handler) Server {
	return &server{
		handler: handler,
	}
}

func (s *server) Start() {
	functions.HTTP("Double", s.handler.Double)
	fmt.Println("server is listening")
	funcframework.Start("8080")
}
