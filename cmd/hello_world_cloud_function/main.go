package main

import (
	"lingbook.io/hello-world-cloud-function/internal/logic"
	"lingbook.io/hello-world-cloud-function/internal/service"
)

func main() {
	mathLogic := logic.NewMath()
	handler := service.NewHandler(mathLogic)
	server := service.NewServer(handler)
	server.Start()
}
